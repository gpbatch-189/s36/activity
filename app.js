const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoutes = require('./routes/taskRoutes.js')

const app = express()
const port = 3001

// Initialize dotenv for usage
dotenv.config()

// Express Setup
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Mongoose Setup
mongoose.connect(`mongodb+srv://admin123:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.1tup4.mongodb.net/S36-Activity?retryWrites=true&w=majority`, {
	useNewUrlParser : true,
	useUnifiedTopology : true
})

let db = mongoose.connection
db.on('error', () => console.error('Connection Error:('))
db.on('open', () => console.log('Connected to MongoDB!'))


// Routes
app.use('/api/tasks', taskRoutes)
// Make sure to assign a specific endpoint for every database collection. In this case, since we are manipulating the 'tasks' collection in MongoDB, then we are specifying an endpoint with specific routes and controllers for that collection.



// Listen to port
app.listen(port, () => console.log(`Server is running at port ${port}`))
