// Import express and any controllers using the 'require' directive
const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()

// Route for /api/tasks/ - Runs the getALLTasks function from the controller
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((tasks) => response.send(tasks))
})


// Route for /api/create - Runs the createTasks function from the controller
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((task) => response.send(task))
})


router.put('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((updatedTask) => response.send(updatedTask))
})


// Route for /api/tasks/:id - Runs the getTask function from the controller
router.get('/:id', (request, response) => {
	TaskController.getTask(request.params.id).then((Task) => response.send(Task))
})

module.exports = router