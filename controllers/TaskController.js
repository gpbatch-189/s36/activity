// Import the Task model from the models folder by using the 'require' directive
const Task = require('../models/Task.js')

// Retrieves all the tasks inside the MondoDB collection
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// Creates a new task inside the MongoDB collection
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			return error
		}
		return 'Task created successfully!'
	})
}

// Updates an existing task inside the MongoDB collection
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error){
			return error
		}

		result.status = newContent.status
	

		return result.save().then((updatedTask, error) => {
			if(error){
				return error
			}

			return updatedTask
		})
	})
}



// Retrieves single task from the MongoDB Collection
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((Task, error) => {
		if (error){
				return error
			}

			return Task
	})
}


